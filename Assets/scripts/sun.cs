﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sun : MonoBehaviour
{
	public float spawnTime = 5f;        // The amount of time between each spawn.
	public float spawnDelay = 3f;       // The amount of time before spawning starts.
	public GameObject ray;

	public void StartSpawning()
	{
		InvokeRepeating("Spawn", spawnDelay, spawnTime);
	}

	void Spawn()
	{
		GameObject GO = Instantiate(ray, Vector3.zero, Quaternion.identity,this.transform);
		GO.name = "fireball";
		GO.transform.localPosition = Vector3.zero;
		GO.transform.localEulerAngles = new Vector3(0, 0, Random.Range(-125, -170));
	}

	public void GameOver()
	{
		CancelInvoke("Spawn");
	}
}
