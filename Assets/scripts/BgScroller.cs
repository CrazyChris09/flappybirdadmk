﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgScroller : MonoBehaviour
{
    public float Speed;
    public bool isDied,isStarted;
    public GameObject flag,AdmkFlag;
    public GameObject flagStop;
    public SpriteRenderer Amma, Mgr;

    void Update()
    {
        if (isStarted == true && isDied == false)
        {
            this.transform.position += Vector3.left * Time.deltaTime * Speed;
        }
    }
}
