﻿using UnityEngine;
using System.Collections;

public class PipeMove : MonoBehaviour {

	public float moveSpeed;
	public Sprite[] sprites;

	// Use this for initialization
	void Start () {
		//this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
		Rigidbody2D body = transform.GetComponent<Rigidbody2D>();
		body.velocity = new Vector2(moveSpeed, 0);
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x <= -0.4) 
		{
			Destroy(gameObject);
		}
	}

	public void GameOver()
	{
		Rigidbody2D body = transform.GetComponent<Rigidbody2D>();
		body.velocity = new Vector2(0, 0);
	}
}
