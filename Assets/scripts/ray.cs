﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ray : MonoBehaviour
{
    public float Speed;

    void Update()
    {
        this.transform.position += transform.right * Time.deltaTime * Speed;
    }
}
