﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BirdControl : MonoBehaviour {

	public int rotateRate = 10;
	public float upSpeed = 10;
    public GameObject scoreMgr;

    public AudioClip jumpUp;
    public AudioClip hit;
    public AudioClip score;
	public GameMain gameMain;

	public Vector3 TargetStartPos,TargetEndPos;

    public bool inGame = false;

	public bool dead = false;
	public bool win = false;
	private bool landed = false;

    private Sequence birdSequence;

    void Start () {
        float birdOffset = 0.05f;
        float birdTime = 0.3f;
        float birdStartY = transform.position.y;

		this.GetComponent<Animator>().speed = 5f;

        birdSequence = DOTween.Sequence();

        birdSequence.Append(transform.DOMoveY(birdStartY + birdOffset, birdTime).SetEase(Ease.Linear))
            .Append(transform.DOMoveY(birdStartY - 2 * birdOffset, 2 * birdTime).SetEase(Ease.Linear))
            .Append(transform.DOMoveY(birdStartY, birdTime).SetEase(Ease.Linear))
            .SetLoops(-1);
    }
	
	// Update is called once per frame
	void Update () {
        if (!inGame)
        {
            return;
        }
        birdSequence.Kill();

		if (dead == false && win == false)
		{
			if (Input.GetButtonDown("Fire1"))
			{
				Debug.Log(1);
                JumpUp();
			}
		}

		if (!landed)
		{
			float v = transform.GetComponent<Rigidbody2D>().velocity.y;
			
			float rotate = Mathf.Min(Mathf.Max(-90, v * rotateRate + 60), 30);
			
			//transform.rotation = Quaternion.Euler(0f, 0f, rotate);
		}
		else
		{
			transform.GetComponent<Rigidbody2D>().rotation = -90;
		}
	}

	[System.Obsolete]
	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log(other.name);
		if (other.name == "land" || other.name == "pipe_up" || other.name == "pipe_down" || other.name == "fireball" || other.name == "cloud")
		{
			if (!dead)
			{
				GameObject[] objs = GameObject.FindGameObjectsWithTag("movable");
				foreach (GameObject g in objs)
				{
					g.BroadcastMessage("GameOver");
				}

				GetComponent<Animator>().SetTrigger("die");
				AudioSource.PlayClipAtPoint(hit, Vector3.zero);

				Sequence sequence = DOTween.Sequence();
				sequence.InsertCallback(0, () => gameMain.Bg.isDied = true);
				sequence.InsertCallback(3f, () => Application.LoadLevel("StartScene"));
			}

			if (other.name == "land")
			{
				transform.GetComponent<Rigidbody2D>().gravityScale = 0;
				transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

				landed = true;
			}
		}

		if (other.name == "AlmostWin")
		{
			Sequence sequence = DOTween.Sequence();
			gameMain.sun.GameOver();
			gameMain.pipeSpawner.GetComponent<PipeSpawner>().GameOver();
		}

		if (other.name == "Win")
		{
			win = true;
			transform.GetComponent<Rigidbody2D>().gravityScale = 0;
			transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
			transform.GetComponent<Rigidbody2D>().isKinematic = true;
			transform.GetComponent<CircleCollider2D>().enabled = false;


			Sequence sequence = DOTween.Sequence();
			sequence.InsertCallback(0, () => gameMain.audioManager.BgSource.Stop());
			sequence.InsertCallback(0, () => gameMain.Bg.isDied = true);
			sequence.InsertCallback(1, () => this.transform.DOMove(TargetStartPos, 1f).SetEase(Ease.Linear));
			sequence.InsertCallback(3, () => this.transform.DOMove(TargetEndPos, 3f).SetEase(Ease.Linear));
			sequence.InsertCallback(9, () => GetComponent<SpriteRenderer>().enabled = false);
			sequence.InsertCallback(9, () => gameMain.Man.SetActive(true));
			sequence.InsertCallback(9, () => gameMain.audioManager.WinSource.Play());
			sequence.InsertCallback(9, () => gameMain.Man.GetComponent<Animator>().SetBool("IsRunning", true));
			sequence.InsertCallback(9, () => gameMain.Man.GetComponent<Animator>().speed = 0.25f);
			sequence.InsertCallback(9, () => gameMain.Bg.transform.DOMove(new Vector3(-33.3f,2.96f,0f),5f).SetEase(Ease.Linear));
			sequence.InsertCallback(9, () => gameMain.Man.transform.position = new Vector3(1.26f,0.84f,0f));
			sequence.InsertCallback(9, () => gameMain.Man.transform.DOMove(new Vector3(0.35f,0.84f,0f),1f).SetEase(Ease.Linear));
			sequence.InsertCallback(10, () => gameMain.Man.transform.DOMove(new Vector3(0.92f,0.84f,0f),2f).SetEase(Ease.Linear));
			sequence.InsertCallback(10, () => gameMain.Bg.AdmkFlag.transform.DOLocalMoveY(0.4f,2f).SetEase(Ease.Linear));
			sequence.InsertCallback(13, () => gameMain.Man.GetComponent<Animator>().SetBool("IsRunning", false));
			sequence.InsertCallback(13, () => gameMain.audioManager.WinSource.Stop());
			sequence.InsertCallback(13, () => gameMain.audioManager.AmmaSpeechSource.Play());
			sequence.InsertCallback(13, () => gameMain.Bg.flag.transform.DOLocalMoveY(-24.6f, 3f));
			sequence.InsertCallback(16, () => gameMain.Bg.Amma.DOFade(1, 0.5f));
			sequence.InsertCallback(16, () => gameMain.Bg.Mgr.DOFade(1, 0.5f));
			sequence.InsertCallback(16+gameMain.audioManager.AmmaSpeechSource.clip.length, () => gameMain.audioManager.AmmaSpeechSource.Stop());
			sequence.InsertCallback(16+gameMain.audioManager.AmmaSpeechSource.clip.length + 1.5f, () => Application.LoadLevel("StartScene"));
		}

		if (other.name == "pass_trigger")
		{
			scoreMgr.GetComponent<ScoreMgr>().AddScore();
			AudioSource.PlayClipAtPoint(score, Vector3.zero);
		}
	}

    public void JumpUp()
    {
        transform.GetComponent<Rigidbody2D>().velocity = new Vector2(0, upSpeed);
        AudioSource.PlayClipAtPoint(jumpUp, Vector3.zero);
    }
	
	public void GameOver()
	{
		dead = true;
	}
}
